from django import forms 
from .models import add_course,add_experience

class AddCourseForm(forms.ModelForm):
    class Meta():
        model = add_course
        fields = ["course_title","course_category","course_price","sale_price","course_type","description","total_hours" ,"cover_image"]
        
class add_experienceForm(forms.ModelForm):
    class Meta():
        model = add_experience
        fields = ["from_year","to","position","organization_name","description",]
        