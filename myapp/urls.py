from django.urls import path
from myapp import views

app_name="myapp"

urlpatterns = [
    path('courses', views.courses, name="courses"),
    path('about', views.about, name="about"),
    path('contact', views.contact, name="contact"),
    path('signup', views.signup, name="signup"),
    path('check_user', views.check_user, name="check_user"),
    path('user_login', views.user_login, name="user_login"),
    path('dashboard', views.dashboard, name="instructor_dashboard"),
    path('instructor_profile', views.instructor_profile, name="instructor_profile"),
    path('us_logout', views.us_logout, name="us_logout"),
    path('inst_add_course', views.inst_add_course, name="inst_add_course"),
    path('instructor_courses', views.instructor_courses, name="instructor_courses"),
    path('edit_course', views.edit_course, name="edit_course"),
    path('delete_topic', views.delete_topic, name="delete_topic"),
    path('add_inst_experience', views.add_inst_experience, name="add_inst_experience"),
    path("change_pass",views.change_pass,name="change_pass"),
    path("single_course",views.single_course,name="single_course"),
    path("get_course_data",views.get_course_data,name="get_course_data"),
    path("cart",views.add_cart,name="cart"),
    path("process_payment",views.process_payment,name="process_payment"),
    path("student_orders",views.student_orders,name="student_orders"),
    path("pending_orders",views.pending_orders,name="pending_orders"),
    path("remove_cart",views.remove_cart,name="remove_cart"),
    path("forgot_pass",views.forgot_pass,name="forgot_pass"),
    path("verify_otp",views.verify_otp,name="verify_otp"),
    path("student_single_course",views.student_single_course,name="student_single_course"),
    path("my_students",views.my_students,name="my_students"),
]
