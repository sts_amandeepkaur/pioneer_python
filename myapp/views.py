from django.shortcuts import render, reverse, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import User
from .models import (AddCategory,Instructor_Profile,add_city,
                    add_course,course_content,add_experience,cart,
                    Order,Contact_us)
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.decorators import login_required
from .forms import AddCourseForm,add_experienceForm
from paypal.standard.forms import PayPalPaymentsForm
from django.core.mail import EmailMessage
import random

def get_user(rq):
    usr = User.objects.get(id=rq.user.id)
    return usr
def index(request):
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    pop = AddCategory.objects.filter().order_by("id")[:6]
    cats = AddCategory.objects.filter().order_by("id")
    if "status" in request.GET: 
        return render(request,"index.html",{"cats":cats,"recent":recent,"response":"Invalid Username or Password","status":"Invalid"})    
    
    return render(request,"index.html",{"cats":cats,"recent":recent,"pop":pop})

def courses(request):
    context={}
    cats = AddCategory.objects.filter().order_by("id")
    all = add_course.objects.all().order_by("course_title")
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    context["cats"] = cats
    context["all"] = all
    return render(request,"courses.html",context)

def about(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    cats = AddCategory.objects.filter().order_by("category_title")
    return render(request,"about.html",{"cats":cats})

def contact(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if request.method=="POST":
        n = request.POST["name"]
        e= request.POST["email"]
        s= request.POST["subject"]
        m= request.POST["message"]
        data = Contact_us(name=n,email=e,subject=s,message=m)
        data.save()
        context["status"] = "Dear {}, Thanks for your feedback!!!".format(n)
    return render(request,"contact.html",context)

def signup(request):
    if request.method=="POST":
        name = request.POST["name"]
        email = request.POST["email"]
        password= request.POST["password"]
        user_type = request.POST["user_type"]

        check = User.objects.filter(username=email)
        if len(check)<1:
            user = User.objects.create_user(email,email,password)
            user.first_name = name
            if user_type=="instructor":
                user.is_staff=True
            user.save()
            return render(request,"index.html", {"response":"Registered Successfully","status":1})
        else:
            return render(request,"index.html",{"response":"A User with this name already exists","status":1})

def check_user(request):
        email = request.GET.get("email")
        check = User.objects.filter(username=email)
        if len(check)==1:
            return HttpResponse(1)
        else:
            return HttpResponse(0)

def user_login(request):
    if request.method=="POST":
        username = request.POST["login_email"]
        passw = request.POST["login_pass"]

        user = authenticate(username=username,password=passw)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                return HttpResponseRedirect("/myapp/dashboard")
            # elif user.is_active:
            #     return HttpResponse("<h1>Student dashboard</h1>")
        else:
            return HttpResponseRedirect("/?status=invalid")
    return HttpResponse("")


#### INSTRUCTOR DASHBOARD
@login_required
def dashboard(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    exp = add_experience.objects.filter(user_id=request.user.id)
    courses = add_course.objects.filter(inst_id__id=request.user.id).order_by("-id")
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    context["courses"] = courses
    context["experience"] =exp
    return render(request,"dashboard/dashboard.html",context)


@login_required    
def instructor_profile(request):
    recent = AddCategory.objects.filter().order_by("-id")[:6]
 
    all_inst = Instructor_Profile.objects.all().exclude(user_id__id=request.user.id).order_by("-id")[:5]
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    city = add_city.objects.all().order_by("city_name")
    exist=False
    data={}
    if len(check)>0:
        exist=True
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
    
    if request.method=="POST":
        name = request.POST["name"]
        email = request.POST["email"]
        contact = request.POST["contact"]
        if contact == "":
            contact=0
        cit = request.POST["city"]
        cp = request.POST["cp"]
        qual = request.POST["qual"]
        address = request.POST["address"]
        instagram = request.POST["instagram"]
        facebook = request.POST["facebook"]
        twitter = request.POST["twitter"]
        about = request.POST["about"]

        user = User.objects.get(id=request.user.id)
        user.first_name = name
        user.email = email
        user.save()

        ct = add_city.objects.get(id=cit)

        if exist==False:
            instructor = Instructor_Profile(user_id=user,contact=contact,position=cp,
                qualification=qual,city_id=ct,full_address=address,facebook_address=facebook,
                twitter_address=twitter,instagram_address=instagram,about=about)
            
            instructor.save()

            if "myimage" in request.FILES:
                image = request.FILES["myimage"]
                instructor.profile_picture = image
            
            instructor.save()
        elif exist==True:

            instructor = Instructor_Profile.objects.get(user_id=user)
            instructor.contact = contact
            instructor.position = cp
            instructor.qualification = qual
            instructor.full_address = address
            instructor.facebook_address = facebook
            instructor.twitter_address = twitter
            instructor.instagram_address = instagram 
            instructor.about = about
            instructor.city_id = ct
            instructor.save()
            if "myimage" in request.FILES:
                image = request.FILES["myimage"]
                instructor.profile_picture = image
            instructor.save()

        
        return render(request,"dashboard/user.html",{"city":city,"inst":data,"status":"Updated Successfully","all_inst":all_inst})

    return render(request,"dashboard/user.html",{"city":city,"inst":data,"all_inst":all_inst,"recent":recent})

@login_required
def us_logout(request):
    logout(request)
    return HttpResponseRedirect("/")

@login_required
def inst_add_course(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    form = AddCourseForm()
    if request.method=="POST":
        form = AddCourseForm(request.POST,request.FILES)
        if form.is_valid():
            form=form.save(commit=False)
            usr = User.objects.get(id=request.user.id)
            form.inst_id = usr
            form.save()
            if "cover_image"in request.FILES:
                print('file=',request.FILES)
                img = request.FILES.get("cover_image")
                form.cover_image=img
                form.save()
            return HttpResponseRedirect("/myapp/edit_course?cid="+str(form.id))
    context.update({"form":form})
    return render(request,"dashboard/add_course.html",context)

@login_required
def instructor_courses(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    courses = add_course.objects.filter(inst_id__id=request.user.id).order_by("-id")

    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    context["courses"]=courses
    if "search" in request.POST:
        qry = request.POST["search"]
        crs = courses.filter(course_title__contains=qry)|courses.filter(course_category__category_title__contains=qry)
        context["courses"]=crs
    return render(request,"dashboard/instructor_courses.html",context)

@login_required
def edit_course(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    crs = {}
    if request.method=="GET":
        if "cid"in request.GET:
            global courseid
            courseid = request.GET["cid"]
            crs = add_course.objects.get(id=courseid)
        if "title" in request.GET:
            id = request.GET["id"]
            crs = add_course.objects.get(id=id)
            title = request.GET["title"]
            price = request.GET["price"]
            sp = request.GET["sp"]
            des = request.GET["des"]

            crs.course_title = title 
            crs.course_price=price
            crs.sale_price=sp
            crs.description=des
            crs.save()
            return HttpResponse("Changes saved !!!")
    contents = course_content.objects.filter(course_id__id=courseid)
    if request.method=="POST":
        topic = request.POST["topic"]
        content = request.POST["content"]
        id = request.POST["cid"]
        course = add_course.objects.get(id=id)
        con_obj = course_content(course_id=course,topic_name=topic,content=content)
        con_obj.save()
        if "video" in request.FILES:
            vid = request.FILES["video"]
            con_obj.file = vid
            con_obj.save()

        context["status"] = "Topic Added!!!"
    context.update({"inst":data,"course":crs,"topics":contents})
    return render(request,"dashboard/edit_course.html",context)

def delete_topic(request):
    id = None
    if "course_id" in request.GET:
        id = request.GET["course_id"]
        add_course.objects.get(id=id).delete()

    elif "topic_id" in request.GET:
        id = request.GET["topic_id"]
        obj=course_content.objects.get(id=id)
        obj.delete()
    return HttpResponse(" Deleted Successfully!!!")

def add_inst_experience(request):
    form = add_experienceForm()
    context = {"form":form}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    if request.method=="POST":
        form = add_experienceForm(request.POST)
        if form.is_valid():
            data=form.save(commit=False)
            usr = User.objects.get(id=request.user.id)
            data.user_id = usr
            data.save()
            context["status"] = "Experience Added Successfully!!!"
    return render(request,"dashboard/add_experience.html",context)

def change_pass(request):
    context = {}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if request.method=="POST":
        old =request.POST["old"]
        nw = request.POST["new"]
        usr = User.objects.get(id=request.user.id)
        check = usr.check_password(old)
        un = usr.username
        if check==True:
            usr.set_password(nw)
            usr.save()
            user=authenticate(username=un,password=nw)
            login(request,user)
            context["msz"]= "Password Changed Successfully"
            context["status"]="alert-success"
        else:
            context["msz"]= "Incorrect Current Password"
            context["status"]="alert-danger"
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)>0:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"]=data
    return render(request,"dashboard/changepass.html",context)

def single_course(request):
    context = {}
    id=request.GET["cid"]
    course = add_course.objects.get(id=id)
    cc = course_content.objects.filter(course_id__id=id)
    context["cc"]=cc
    context["course"]=course
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if request.user.is_staff:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    temp ="dashboard/single_course.html"
    if "type" in request.GET:
        temp = "singlecourse.html"
        data = Instructor_Profile.objects.get(user_id__id=course.inst_id.id)
        context["inst"] = data
    return render(request,temp,context)

def get_course_data(request):
    id = request.GET["cat_id"]
    data = []
    if id=="None":
        data = list(add_course.objects.all().order_by("course_title").values())
    else:
        data = list(add_course.objects.filter(course_category__id=id).values())
    return JsonResponse({"data":data})

def add_cart(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    context["items"] = items

    if request.method=="POST":
        if request.user.is_authenticated:
            cid = request.POST["course_id"]
            obj =  get_object_or_404(add_course,id=cid)
            exist = cart.objects.filter(user_id__id=request.user.id,course_id=cid,status=False)
            ordered = cart.objects.filter(user_id__id=request.user.id,course_id=cid,status=True)
            if len(ordered)>0:
                context.update({"status":"Course already puchased","class":"alert-primary"})
            elif len(exist)<1:
                usr = User.objects.get(id=request.user.id)
                data = cart(user_id=usr,course_id=obj)
                data.save()
                context.update({"status":"Course Added In your Cart","class":"alert-success"})
            else:
                context.update({"status":"Course already in your Cart","class":"alert-warning"})
        else:
            context.update({"status":"You need to Login First","class":"alert-danger"})    
    return render(request,"cart.html",context)

def process_payment(request):
    context = {}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    pr = ""
    amt = 0
    inv = "100"
    crs=""
    cart_id = ""
    for j in items:
        pr += str(j.course_id.course_title)  
        amt += j.course_id.sale_price
        inv += str(j.id)
        crs += str(j.course_id.id)+","
        cart_id += str(j.id)+","
    paypal_dict = {
        "business": "sts.amandeepkaur@gmail.com",
        "amount": str(amt),
        "item_name": pr,
        "invoice": inv,
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return": request.build_absolute_uri(reverse('ps')),
        "cancel_return": request.build_absolute_uri(reverse('pf')),
        "custom": "premium_plan",  # Custom command to correlate to some function later (optional)
    }
    usr = get_object_or_404(User,id=request.user.id)
    ch=Order.objects.filter(Invoice_id=inv)
    if len(ch)<1:
        order = Order(cust_id=usr,course_ids=crs,cart_ids=cart_id,amount_total=amt,Invoice_id=inv)
        order.save()
        request.session["order_id"] = order.id
    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context.update({"form": form})
    return render(request,"process_payment.html",context)
    

def p_success(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord = get_object_or_404(Order,id=order_id)
        ord.status = True
        ord.save()

        cids = [i for i in ord.cart_ids.split(",")]
        for i in cids[:-1]:
            cart_obj = cart.objects.get(id=i,status=False)
            cart_obj.status = True 
            cart_obj.save()            

        return render(request,"success_payment.html")
    else:
        return HttpResponse("<h1>SOMETHING WENT WRONG</h1>")
def p_fail(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    return render(request,"payment_failed.html")

def student_orders(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    courses = []
    cid =[]
    orders = Order.objects.filter(cust_id=request.user.id,status=True)
    for i in orders:
        for ab in i.course_ids.split(",")[:-1]:
            cid.append(ab)
    for i in cid:
        courses.append(add_course.objects.get(id=i))
    context["courses"] = courses
    return render(request,"dashboard/student_orders.html",context)

def pending_orders(request):
    context={}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    ords = []
    orders = Order.objects.filter(cust_id=request.user.id,status=False)
    crs=[]
    for i in orders:
        ids = [j for j in i.course_ids.split(",")]
        for id in ids[:-1]:
            crs.append(add_course.objects.get(id=id))
        c = {
            "id":i.id,
            "course":crs,
            "amount_total":i.amount_total,
            "invoice":i.Invoice_id,
            "status":i.status,
            "order_date":i.created_on
        }
        ords.append(c)
    context["orders"] = ords
    return render(request,"dashboard/pending_orders.html",context)

def remove_cart(request):
    id = request.GET["cart_id"]
    cobj=get_object_or_404(cart,id=id)
    cobj.delete()
    return HttpResponseRedirect("/myapp/cart#mycart")

def forgot_pass(request):
    context = {}
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["password"]
        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()
        context["status"]= "Password Changed Successfully!!!"
    return render(request,"forgot_pass.html",context)


def verify_otp(request):
    if "username" in request.GET:
        u = request.GET["username"]
        try:
            user = User.objects.get(username=u)
            otp = random.randint(1000,9999)
            m = "Dear {},\n {} is your One Time Password (OTP) to recover your password. Fo not share it with others.\n Thanks & Regards \n PIONEER PYTHON TEAM ".format(user.first_name,otp)
            snd = send_msz("Account Verification",m,user.email)
            if snd == "success":
                return JsonResponse({"email":user.email,"status":"success","myotp":otp})
            else:
                return JsonResponse({"email":user.email,"status":"send_fail"})    
        except:
            return JsonResponse({"status":"failed"})

def send_msz(sub,msz,rec):
    try:
        e=EmailMessage(sub,msz,to=[rec,])
        e.send()
        return "success"
    except:
        return "error"

def student_single_course(request):
    context = {}
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    recent = AddCategory.objects.filter().order_by("-id")[:6]
    context["recent"] = recent
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
        
    id=request.GET["cid"]
    course = add_course.objects.get(id=id)
    cc = course_content.objects.filter(course_id__id=id)
    context["cc"]=cc
    context["course"]=course
    return render(request,"dashboard/student_single_course.html",context)

def my_students(request):
    context = {}
    students = cart.objects.filter(course_id__inst_id__id=request.user.id, status=True)
    check = Instructor_Profile.objects.filter(user_id__id=request.user.id)
    st_ids = list(set([i.user_id.id for i in students]))
    
    if len(check)==1:
        data = Instructor_Profile.objects.get(user_id__id=request.user.id)
        context["inst"] = data
    
    sts = User.objects.filter(id__in=st_ids)
    details = []
    for id in sts:
        usr = {}
        user = get_object_or_404(User,id=id.id)
        usr["name"] = user.first_name
        usr["email"] = user.email
        ch=Instructor_Profile.objects.filter(user_id__id=id.id)
        if len(ch)>0:
            profile = get_object_or_404(Instructor_Profile,user_id__id=id.id)
            usr["image"] = profile.profile_picture
            usr["qualification"] = profile.qualification
            usr["position"] = profile.position
            
        
        details.append(usr)
        print(details)
    context["students"] = details

    return render(request,"dashboard/my_students.html",context)