from django.contrib import admin

admin.site.site_header="PIONEER PYTHON | Admin Login"

from .models import (AddCategory,Instructor_Profile,add_city,
        add_course,course_content, add_experience,cart,Order,Contact_us)

class AddCategoryAdmin(admin.ModelAdmin):
    list_display = ["id","category_title","description","added_on"]

class add_cityAdmin(admin.ModelAdmin):
    list_display = ["id","city_name","pin_code","state","country","added_on"]

class Instructor_ProfileAdmin(admin.ModelAdmin):
    list_display = ["id","contact","position","qualification","about","added_on","updated_on"]

class add_courseAdmin(admin.ModelAdmin):
    list_display = ["id","course_title","course_category","course_price","sale_price","course_type","total_hours","added_on"]

class Contact_usAdmin(admin.ModelAdmin):
    list_display = ['id','name','email','subject','message','received_on']

class add_experienceAdmin(admin.ModelAdmin):
    list_display= ["id","user_id","from_year","to","position","organization_name","description","added_on"]

class cartAdmin(admin.ModelAdmin):
    list_display= ["id","user_id","course_id","status","type","create_date","updated_on"]

class OrderAdmin(admin.ModelAdmin):
    list_display = ["id","cust_id","course_ids","cart_ids","amount_total","Invoice_id","status","created_on"]
admin.site.register(AddCategory,AddCategoryAdmin)
admin.site.register(add_city,add_cityAdmin)
admin.site.register(Instructor_Profile,Instructor_ProfileAdmin)
admin.site.register(add_course,add_courseAdmin)
admin.site.register(course_content)
admin.site.register(add_experience,add_experienceAdmin)
admin.site.register(cart,cartAdmin)
admin.site.register(Order,OrderAdmin)
admin.site.register(Contact_us,Contact_usAdmin)
