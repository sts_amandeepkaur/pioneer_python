from django.db import models
from django.contrib.auth.models import User

class AddCategory(models.Model):
    category_title = models.CharField(max_length=250)
    cover_picture = models.FileField(upload_to="categories/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)    

    def __str__(self):
        return self.category_title

    class Meta:
        verbose_name_plural = "Add Category"

class add_city(models.Model):
    city_name = models.CharField(max_length=100)
    pin_code = models.IntegerField()
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    added_on =  models.DateTimeField(auto_now_add=True)

class Instructor_Profile(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    contact = models.IntegerField(blank=True)
    profile_picture = models.FileField(upload_to="instructors/%Y/%m/%d",blank=True)
    position = models.CharField(max_length=200,blank=True)
    qualification = models.CharField(max_length=100,blank=True)
    city_id = models.ForeignKey(add_city,on_delete=models.CASCADE,blank=True)
    full_address = models.TextField(blank=True)
    facebook_address = models.URLField(blank=True)
    twitter_address = models.URLField(blank=True)
    instagram_address = models.URLField(blank=True)
    about = models.TextField(blank=True)
    added_on =  models.DateTimeField(auto_now_add=True)
    updated_on =  models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user_id.first_name

class add_course(models.Model):
    ch = (
        ("Free","Free"),
        ("Paid","Paid")
    )
    inst_id = models.ForeignKey(User,on_delete =models.CASCADE,null=True)
    course_title =models.CharField(max_length=250)
    course_category = models.ForeignKey(AddCategory, on_delete =models.CASCADE)
    course_price = models.FloatField()
    sale_price = models.FloatField(blank=True,default=0)
    cover_image = models.FileField(upload_to="courses/%Y/%m/%d")
    course_type = models.CharField(max_length=100,choices=ch)
    description = models.TextField()
    course_curriculum = models.TextField(blank=True)
    total_hours = models.IntegerField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.course_title

class course_content(models.Model):
    course_id = models.ForeignKey(add_course,on_delete =models.CASCADE)
    topic_name = models.CharField(max_length=500)
    content = models.TextField(blank=True)
    file = models.FileField(upload_to="course_content/%Y/%m/%d",blank=True,null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    
    def __str__(self):
        return self.topic_name

class add_experience(models.Model):
    user_id= models.ForeignKey(User,on_delete =models.CASCADE,null=True)
    from_year = models.CharField(max_length=250)
    to = models.CharField(max_length=250)
    position = models.CharField(max_length=500)
    organization_name = models.CharField(max_length=500)
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    
    def __str__(self):
        return self.user_id.username

class cart(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    course_id = models.ForeignKey(add_course, on_delete=models.CASCADE,null=True)
    status = models.BooleanField(default=False)
    type=models.IntegerField(null=True,blank=True,default=0)
    create_date = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.user_id.username

class Order(models.Model):
    cust_id = models.ForeignKey(User,on_delete = models.CASCADE)
    course_ids = models.CharField(max_length=500,null=True)
    cart_ids = models.CharField(max_length=500,null=True)
    amount_total = models.FloatField(null=True)
    Invoice_id = models.CharField(max_length=500)
    status = models.BooleanField(default = False)
    created_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.cust_id.username

class Contact_us(models.Model):
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    received_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.name

