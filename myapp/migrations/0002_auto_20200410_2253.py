# Generated by Django 3.0.4 on 2020-04-10 17:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='add_city',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city_name', models.CharField(max_length=100)),
                ('pin_code', models.IntegerField()),
                ('state', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='addcategory',
            options={'verbose_name_plural': 'Add Category'},
        ),
        migrations.CreateModel(
            name='Instructor_Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact', models.IntegerField(blank=True)),
                ('profile_picture', models.FileField(blank=True, upload_to='instructors/%Y/%m/%d')),
                ('position', models.CharField(blank=True, max_length=200)),
                ('qualification', models.CharField(blank=True, max_length=100)),
                ('full_address', models.TextField(blank=True)),
                ('facebook_address', models.URLField(blank=True)),
                ('twitter_address', models.URLField(blank=True)),
                ('instagram_address', models.URLField(blank=True)),
                ('about', models.TextField(blank=True)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('city_id', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='myapp.add_city')),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
