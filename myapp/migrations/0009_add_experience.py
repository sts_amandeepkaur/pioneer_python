# Generated by Django 3.0.4 on 2020-04-18 20:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0008_auto_20200419_0013'),
    ]

    operations = [
        migrations.CreateModel(
            name='add_experience',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_year', models.CharField(max_length=250)),
                ('to', models.CharField(max_length=250)),
                ('position', models.CharField(max_length=500)),
                ('organization_name', models.CharField(max_length=500)),
                ('description', models.TextField()),
                ('added_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
