from django.contrib import admin
from django.urls import path,include
from myapp import views
from django.conf import settings 
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path("myapp/",include("myapp.urls")),

    path('paypal/', include('paypal.standard.ipn.urls')),

    path('ps/',views.p_success,name='ps'),
    path('pf/',views.p_fail, name='pf' ),

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
